package com.nalyvaiko.buildings;

/**
 * Building class designed for represent building model.
 *
 * @author Orest Nalyvaiko
 */
public abstract class Building {

  /**
   * Integer variable designed for description of rental price.
   */
  private int rentalPrice;
  /**
   * Integer variable designed for description of distance to the child garden.
   */
  private int distanceToChildGarden;
  /**
   * Integer variable designed for description of distance to the school.
   */
  private int distanceToSchool;
  /**
   * Integer variable designed for description of distance to the playground.
   */
  private int distanceToPlayground;

  /**
   * Constructor.
   *
   * @param rentPrice rental price of the building
   * @param distanceChildGarden distance to the child garden
   * @param distanceSchool distance to the school
   * @param distancePlayground distance to the playground
   */
  Building(final int rentPrice, final int distanceChildGarden,
      final int distanceSchool, final int distancePlayground) {
    this.rentalPrice = rentPrice;
    this.distanceToChildGarden = distanceChildGarden;
    this.distanceToSchool = distanceSchool;
    this.distanceToPlayground = distancePlayground;
  }

  /**
   * Getter.
   *
   * @return rental price
   */
  public final int getRentalPrice() {
    return rentalPrice;
  }

  /**
   * Getter.
   *
   * @return distance to child garden
   */
  public final int getDistanceToChildGarden() {
    return distanceToChildGarden;
  }

  /**
   * Getter.
   *
   * @return distance to school
   */
  public final int getDistanceToSchool() {
    return distanceToSchool;
  }

  /**
   * Getter.
   *
   * @return distance to playground
   */
  public final int getDistanceToPlayground() {
    return distanceToPlayground;
  }

  /**
   * Abstract method designed for rent building.
   */
  public abstract void rentBuilding();

  /**
   * Returns a string representation of the object.
   *
   * @return a string representation of the object.
   */
  @Override
  public String toString() {
    return ": rentalPrice = " + rentalPrice
        + "$, distanceToChildGarden = " + distanceToChildGarden
        + "m, distanceToSchool = " + distanceToSchool
        + "m, distanceToPlayground = " + distanceToPlayground + "m";
  }
}

package com.nalyvaiko.buildings;

/**
 * Mansion class designed for specification Building class.
 *
 * @author Orest Nalyvaiko
 */
public class Penthouse extends Building {

  /**
   * Integer variable designed for the description the terrace area.
   */
  private int terraceArea;

  /**
   * Constructor.
   *
   * @param rentalPrice rental price of the building
   * @param distanceToChildGarden distance to the child garden
   * @param distanceToSchool distance to the school
   * @param distanceToPlayground distance to the playground
   * @param terracesArea the terrace area
   */
  public Penthouse(final int rentalPrice, final int distanceToChildGarden,
      final int distanceToSchool, final int distanceToPlayground,
      final int terracesArea) {
    super(rentalPrice, distanceToChildGarden, distanceToSchool,
        distanceToPlayground);
    this.terraceArea = terracesArea;
  }

  /**
   * Method for renting building.Print information.
   */
  @Override
  public final void rentBuilding() {
    System.out.println("Penthouse is successfully rented");
  }

  /**
   * Returns a string representation of the object.
   *
   * @return a string representation of the object.
   */
  @Override
  public final String toString() {
    return "Penthouse" + super.toString() + ", terrace area = "
        + terraceArea + "sq";
  }
}

package com.nalyvaiko.buildings;

/**
 * Mansion class designed for specification Building class.
 *
 * @author Orest Nalyvaiko
 */
public class Mansion extends Building {

  /**
   * Integer variable designed for the description the amount of garages.
   */
  private int amountOfGarage;

  /**
   * Constructor.
   *
   * @param rentalPrice rental price of the building
   * @param distanceToChildGarden distance to the child garden
   * @param distanceToSchool distance to the school
   * @param distanceToPlayground distance to the playground
   * @param amountOfGarages the amount of garages
   */
  public Mansion(final int rentalPrice, final int distanceToChildGarden,
      final int distanceToSchool, final int distanceToPlayground,
      final int amountOfGarages) {
    super(rentalPrice, distanceToChildGarden, distanceToSchool,
        distanceToPlayground);
    this.amountOfGarage = amountOfGarages;
  }

  /**
   * Method for renting building.Print information.
   */
  @Override
  public final void rentBuilding() {
    System.out.println("Mansion is successfully rented");
  }

  /**
   * Returns a string representation of the object.
   *
   * @return a string representation of the object.
   */
  @Override
  public final String toString() {
    return "Mansion  " + super.toString() + ", amount of garage = "
        + amountOfGarage;
  }
}

package com.nalyvaiko.buildings;

/**
 * Apartment class designed for specification Building class.
 *
 * @author Orest Nalyvaiko
 */
public class Apartment extends Building {

  /**
   * Integer variable designed for the description the floor number.
   */
  private int floor;

  /**
   * Constructor.
   *
   * @param rentalPrice rental price of the building
   * @param distanceToChildGarden distance to the child garden
   * @param distanceToSchool distance to the school
   * @param distanceToPlayground distance to the playground
   * @param floorNumber the floor number
   */
  public Apartment(final int rentalPrice, final int distanceToChildGarden,
      final int distanceToSchool, final int distanceToPlayground,
      final int floorNumber) {
    super(rentalPrice, distanceToChildGarden, distanceToSchool,
        distanceToPlayground);
    this.floor = floorNumber;
  }

  /**
   * Method for renting building.Print information.
   */
  @Override
  public final void rentBuilding() {
    System.out.println("Apartment is successfully rented");
  }

  /**
   * Returns a string representation of the object.
   *
   * @return a string representation of the object.
   */
  @Override
  public final String toString() {
    return "Apartment" + super.toString() + ", floor = " + floor;
  }
}

package com.nalyvaiko.service;

import com.nalyvaiko.buildings.Building;
import com.nalyvaiko.creator.Creator;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Command class designed for sorting operations, show all buildings,
 * rent building.
 *
 * @author Orest Nalyvaiko
 */
class Command {

  /**
   * List variable for storing sorted buildings.
   */
  private List<Building> buildings;
  /**
   * Creator object designed for getting created buildings.
   */
  private Creator creator;

  /**
   * Constructor designed for initialization.
   */
  Command() {
    creator = new Creator();
  }

  /**
   * Method designed for getting all buildings.
   *
   * @return list of all buildings
   */
  List<Building> getAllBuildings() {
    buildings = null;
    return creator.getBuildings();
  }

  /**
   * Method designed for sort buildings by rental price.
   *
   * @param rentalPrice rental price
   * @return sorted list
   */
  List<Building> sortBuildingsByRentalPrice(final int rentalPrice) {
    if (buildings == null) {
      buildings = creator.getBuildings().stream()
          .filter(building -> building.getRentalPrice() <= rentalPrice)
          .sorted(Comparator.comparingInt(Building::getRentalPrice))
          .collect(Collectors.toList());
      return buildings;
    } else {
      buildings = buildings.stream()
          .filter(building -> building.getRentalPrice() <= rentalPrice)
          .sorted(Comparator.comparingInt(Building::getRentalPrice))
          .collect(Collectors.toList());
      return buildings;
    }
  }

  /**
   * Method designed for sort buildings by distance to child garden.
   *
   * @param distanceToChildGarden distance to child garden
   * @return sorted list
   */
  List<Building>
      sortBuildingsByDistanceToChildGarden(final int distanceToChildGarden) {
    if (buildings == null) {
      buildings = creator.getBuildings().stream()
          .filter(building -> building.getDistanceToChildGarden()
              <= distanceToChildGarden)
          .sorted(Comparator.comparingInt(Building::getDistanceToChildGarden))
          .collect(Collectors.toList());
      return buildings;
    } else {
      buildings = buildings.stream()
          .filter(building -> building.getDistanceToChildGarden()
              <= distanceToChildGarden)
          .sorted(Comparator.comparingInt(Building::getDistanceToChildGarden))
          .collect(Collectors.toList());
      return buildings;
    }
  }

  /**
   * Method designed for sort buildings by distance to school.
   *
   * @param distanceToSchool distance to school
   * @return sorted list
   */
  List<Building> sortBuildingsByDistanceToSchool(final int distanceToSchool) {
    if (buildings == null) {
      buildings = creator.getBuildings().stream()
          .filter(building -> building.getDistanceToSchool()
              <= distanceToSchool)
          .sorted(Comparator.comparingInt(Building::getDistanceToSchool))
          .collect(Collectors.toList());
      return buildings;
    } else {
      buildings = buildings.stream()
          .filter(building -> building.getDistanceToSchool()
              <= distanceToSchool)
          .sorted(Comparator.comparingInt(Building::getDistanceToSchool))
          .collect(Collectors.toList());
      return buildings;
    }
  }

  /**
   * Method designed for sort buildings by distance to playground.
   *
   * @param distanceToPlayground distance to playground
   * @return sorted list
   */
  List<Building>
      sortBuildingsByDistanceToPlayground(final int distanceToPlayground) {
    if (buildings == null) {
      buildings = creator.getBuildings().stream()
          .filter(building -> building.getDistanceToPlayground()
              <= distanceToPlayground)
          .sorted(Comparator.comparingInt(Building::getDistanceToPlayground))
          .collect(Collectors.toList());
      return buildings;
    } else {
      buildings = buildings.stream()
          .filter(building -> building.getDistanceToPlayground()
              <= distanceToPlayground)
          .sorted(Comparator.comparingInt(Building::getDistanceToPlayground))
          .collect(Collectors.toList());
      return buildings;
    }
  }

  /**
   * Method rent building.
   *
   * @return false - can not rent
   *         true - rent was done successfully
   */
  boolean rentBuilding() {
    if (buildings == null) {
      return false;
    } else {
      buildings.get(0).rentBuilding();
      creator.getBuildings().remove(buildings.get(0));
      buildings.remove(0);
      return true;
    }
  }

  /**
   * Setter.
   *
   * @param buildingList list of buildings
   */
  void setBuildings(final List<Building> buildingList) {
    this.buildings = buildingList;
  }
}

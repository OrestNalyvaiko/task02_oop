package com.nalyvaiko.service;

import com.nalyvaiko.buildings.Building;
import java.util.Scanner;

/**
 * UserService class designed for user service.
 *
 * @author Orest Nalyvaiko
 */
public class UserService {

  /**
   * Command object for getting commands.
   */
  private Command command;

  /**
   * Constructor designed for initialization.
   */
  public UserService() {
    command = new Command();
  }

  /**
   * Method show choices.
   */
  public final void showChoices() {
    System.out.println("1.View all buildings");
    System.out.println("2.Search by rent price");
    System.out.println("3.Search by distance to child garden");
    System.out.println("4.Search by distance to school");
    System.out.println("5.Search by distance to play ground");
    System.out.println("6.Rent building");
    System.out.println("7.Exit");
    System.out.print("Enter your choice: ");
  }

  /**
   * Print all buildings.
   */
  public final void viewAllBuildings() {
    for (Building building : command.getAllBuildings()) {
      System.out.println(building);
    }
  }

  /**
   * Search by rental price.
   */
  public final void searchByRentPrice() {
    System.out.print(
        "Enter integer the rental price in dollars "
            + "which are comfortable for you: ");
    Scanner scanner = new Scanner(System.in, "UTF-8");
    int rentalPrice = scanner.nextInt();
    if (command.sortBuildingsByRentalPrice(rentalPrice).isEmpty()) {
      System.out.println("Such buildings are not in Lviv.");
      command.setBuildings(null);
    } else {
      for (Building building : command
          .sortBuildingsByRentalPrice(rentalPrice)) {
        System.out.println(building);
      }
    }
  }

  /**
   * Search by distance to child garden.
   */
  public final void searchByDistanceToChildGarden() {
    System.out
        .print(
            "Enter integer distance in meters to child garden"
                + " which are comfortable for you: ");
    Scanner scanner = new Scanner(System.in, "UTF-8");
    int distanceToChildGarden = scanner.nextInt();
    if (command
        .sortBuildingsByDistanceToChildGarden(distanceToChildGarden)
        .isEmpty()) {
      System.out.println("Such buildings are not in Lviv.");
      command.setBuildings(null);
    } else {
      for (Building building : command
          .sortBuildingsByDistanceToChildGarden(distanceToChildGarden)) {
        System.out.println(building);
      }
    }
  }

  /**
   * Search by distance to school.
   */
  public final void searchByDistanceToSchool() {
    System.out.print(
        "Enter integer distance in meters to school "
            + "which are comfortable for you: ");
    Scanner scanner = new Scanner(System.in, "UTF-8");
    int distanceToSchool = scanner.nextInt();
    if (command.sortBuildingsByDistanceToSchool(distanceToSchool).isEmpty()) {
      System.out.println("Such buildings are not in Lviv.");
      command.setBuildings(null);
    } else {
      for (Building building : command
          .sortBuildingsByDistanceToSchool(distanceToSchool)) {
        System.out.println(building);
      }
    }
  }

  /**
   * Search by distance to playground.
   */
  public final void searchByDistanceToPlayground() {
    System.out
        .print(
            "Enter integer distance in meters to playground "
                + "which are comfortable for you: ");
    Scanner scanner = new Scanner(System.in, "UTF-8");
    int distanceToPlayground = scanner.nextInt();
    if (command.sortBuildingsByDistanceToPlayground(distanceToPlayground)
        .isEmpty()) {
      System.out.println("Such buildings are not in Lviv.");
      command.setBuildings(null);
    } else {
      for (Building building : command
          .sortBuildingsByDistanceToPlayground(distanceToPlayground)) {
        System.out.println(building);
      }
    }
  }

  /**
   * Rent building.
   */
  public final void rentBuilding() {
    if (!command.rentBuilding()) {
      System.out.println("Can not rent building,try again");
    }
  }
}

package com.nalyvaiko.creator;

import com.nalyvaiko.buildings.Apartment;
import com.nalyvaiko.buildings.Building;
import com.nalyvaiko.buildings.Mansion;
import com.nalyvaiko.buildings.Penthouse;
import java.util.ArrayList;
import java.util.List;

/**
 * Creator class designed for creating list of buildings.
 *
 * @author Orest Nalyvaiko
 */
public class Creator {

  /**
   * List variable for containing buildings.
   */
  private List<Building> buildings;

  /**
   * Constructor for initialization.
   */
  public Creator() {
    buildings = createBuildings();
  }

  /**
   * Method fills in list buildings.
   *
   * @return list of buildings
   */
  private List<Building> createBuildings() {
    List<Building> buildingList = new ArrayList<>();
    buildingList.add(new Penthouse(600, 50, 750, 400, 30));
    buildingList.add(new Apartment(200, 100, 200, 300, 4));
    buildingList.add(new Mansion(400, 200, 300, 500, 1));
    buildingList.add(new Apartment(300, 250, 100, 200, 1));
    buildingList.add(new Penthouse(100, 300, 600, 100, 20));
    buildingList.add(new Mansion(250, 400, 400, 150, 2));
    return buildingList;
  }

  /**
   * Getter.
   *
   * @return list of buildings
   */
  public final List<Building> getBuildings() {
    return buildings;
  }
}

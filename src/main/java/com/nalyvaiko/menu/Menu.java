package com.nalyvaiko.menu;

import com.nalyvaiko.service.UserService;
import java.util.Scanner;

/**
 * Menu class designed for menu.
 *
 * @author Orest Nalyvaiko
 */
public class Menu {

  /**
   * UserService object designed for user service.
   */
  private UserService userService;

  /**
   * Constructor designed for initialization.
   */
  public Menu() {
    userService = new UserService();
  }

  /**
   * Method starting menu.
   */
  public final void start() {
    boolean state = true;
    while (state) {
      userService.showChoices();
      Scanner scanner = new Scanner(System.in, "UTF-8");
      int choice = scanner.nextInt();
      switch (choice) {
        case 1:
          userService.viewAllBuildings();
          break;
        case 2:
          userService.searchByRentPrice();
          break;
        case 3:
          userService.searchByDistanceToChildGarden();
          break;
        case 4:
          userService.searchByDistanceToSchool();
          break;
        case 5:
          userService.searchByDistanceToPlayground();
          break;
        case 6:
          userService.rentBuilding();
          break;
        case 7:
          state = false;
          break;
        default:
          break;
      }
    }
  }
}
